﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotallyAwesomePOS.UI.Model
{
    public class Session : IDisposable
    {
        private static Session _current;
        private readonly Cashier _cashier;

        public Session(Cashier cashier)
        {
            if (cashier == null) throw new ArgumentNullException("cashier");
            _cashier = cashier;
        }

        public static Session Current
        {
            get { return _current; }
        }

        public Cashier Cashier
        {
            get { return _cashier; }
        }

        public void Dispose()
        {
            _current = null;
        }

        public static Session Start(Cashier cashier)
        {
            _current = new Session(cashier);
            return _current;
        }
    }
}
