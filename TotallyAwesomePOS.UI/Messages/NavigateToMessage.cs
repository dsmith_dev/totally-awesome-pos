﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace TotallyAwesomePOS.UI.Messages
{

    /// <summary>
    /// Helper class to make Navigating more elegant
    /// </summary>
    public static class Navigate
    {
        public static NavigateMessage<TViewModel> ToViewModel<TViewModel>(object from)
           where TViewModel : ViewModelBase
        {
            return new NavigateMessage<TViewModel>(from);
        }
    }

    public class NavigateMessage : UIMessage
    {

        private readonly Type _pageOrViewModelType;

        public NavigateMessage(object source, Type pageOrViewModelType)
            : base(source)
        {
            _pageOrViewModelType = pageOrViewModelType;
        }

        public Type PageOrViewModelType
        {
            get { return _pageOrViewModelType; }
        }

       

    }

    public class NavigateMessage<T> : NavigateMessage
    {
        public NavigateMessage(object source)
            : base(source, typeof(T))
        { }


        
    }
}
