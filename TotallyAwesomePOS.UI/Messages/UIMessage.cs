﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotallyAwesomePOS.UI.Messages
{
    /// <summary>
    /// Provides a base class for all UI messages.  All messages should derive from
    /// this class so it is possible to listen to all messages at one.
    /// </summary>
    public class UIMessage
    {

        private readonly object _source;

        public UIMessage(object source)
        {
            if (source == null) throw new ArgumentNullException("source");
            _source = source;
        }

    }
}
