﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Castle.Windsor;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;

using TotallyAwesomePOS.Security;

namespace TotallyAwesomePOS.UI.StartUp
{
    public class SecurityInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {

            container.Register(
                // You would use this when the CashierAuthenticator is actually implemented
                //Component.For<ICashierAuthenticator>()
                //     .ImplementedBy<CashierAuthenticator>()

                // But for purposes of demonstration I am going to use a dummy authenticator for now
                Component.For<ICashierAuthenticator>()
                         .ImplementedBy<DummyAuthenticator>()
                );

        }

    }

    public class DummyAuthenticator : ICashierAuthenticator
    {
        public Cashier Authenticate(string username, string password)
        {
            return new Cashier { Name = username };
        }
    }
}
