﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Castle.Windsor;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;

using GalaSoft.MvvmLight.Messaging;

using TotallyAwesomePOS.UI.Pages;
using TotallyAwesomePOS.UI.ViewModel;
using TotallyAwesomePOS.UI.Model;

namespace TotallyAwesomePOS.UI.StartUp
{


    public class UIInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes.FromThisAssembly()
                       .Where(t => t.Name.EndsWith("Window")),

                Classes.FromThisAssembly()
                       .Where(t => t.Name.EndsWith("ViewModel")),

                Classes.FromThisAssembly()
                       .Where(t => t.Name.EndsWith("Page")),

                Component.For<IMessenger>()
                         .ImplementedBy<Messenger>(),

                Component.For<IPasswordProvider>()
                         .ImplementedBy<LoginPage.LoginPasswordProvider>(),

                Component.For<Session>()
                         .UsingFactoryMethod((c, k) => Session.Current)
                
            );

        }
    }
}
