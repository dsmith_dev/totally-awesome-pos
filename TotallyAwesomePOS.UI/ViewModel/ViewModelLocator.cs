/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:TotallyAwesomePOS.UI"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;

namespace TotallyAwesomePOS.UI.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
        }

        public App App
        {
            get { return (App)Application.Current; }
        }

        public MainViewModel Main
        {
            get
            {
                return App.IoC.Resolve<MainViewModel>();
            }
        }

        public LoginViewModel Login
        {
            get
            {
                return App.IoC.Resolve<LoginViewModel>();
            }
        }

        public HomeViewModel Home
        {
            get
            {
                return App.IoC.Resolve<HomeViewModel>();
            }
        }
        
        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}