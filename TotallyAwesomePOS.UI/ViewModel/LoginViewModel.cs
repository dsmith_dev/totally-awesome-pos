﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using TotallyAwesomePOS.Security;
using TotallyAwesomePOS.UI.Messages;
using TotallyAwesomePOS.UI.Model;

namespace TotallyAwesomePOS.UI.ViewModel
{
    public class LoginViewModel : ViewModelBase
    {

        private string _username;
        private ICommand _loginCommand;
        
        public IPasswordProvider PasswordProvider { get; set; }

        public ICashierAuthenticator CashierAuthenticator { get; set; }

        public IMessenger Messenger { get; set; }
        
        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                RaisePropertyChanged("Username");

                if(_loginCommand != null)
                {
                    (_loginCommand as RelayCommand).RaiseCanExecuteChanged();
                }
            }
        }

        public bool IsValid
        {
            get
            {
                return string.IsNullOrEmpty(Username) == false;
            }
        }

        public ICommand LoginCommand
        {
            get
            {
                if(_loginCommand== null)
                {
                    _loginCommand = new RelayCommand(
                        execute: () => Login(), 
                        canExecute: () => IsValid);
                }
                return _loginCommand;
            }
        }

        protected void Login()
        {
            var password = PasswordProvider.GetPassword();

            var cashier = CashierAuthenticator.Authenticate(Username, password);

            if(cashier != null)
            {
                Session.Start(cashier);
                Messenger.Send(Navigate.ToViewModel<HomeViewModel>(from: this));
            }

        }

    }
}
