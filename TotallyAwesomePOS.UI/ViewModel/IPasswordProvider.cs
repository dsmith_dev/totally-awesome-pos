﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotallyAwesomePOS.UI.ViewModel
{
    public interface IPasswordProvider
    {
        string GetPassword();
    }
}
