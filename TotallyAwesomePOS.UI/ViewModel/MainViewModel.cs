using Castle.MicroKernel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using TotallyAwesomePOS.UI.Messages;

namespace TotallyAwesomePOS.UI.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {

        private object _mainContent;

        public IMessenger Messenger { get; set; }

        public IKernel Kernel { get; set; }

        public object MainContent
        {
            get { return _mainContent; }
            set
            {
                _mainContent = value;
                RaisePropertyChanged("MainContent");
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IMessenger messenger)
        {
            Messenger = messenger;

            Messenger.Register<NavigateMessage>(
                recipient: this, 
                receiveDerivedMessagesToo: true, 
                action: NavigateTo);
            
        }

        protected void NavigateTo(NavigateMessage message)
        {
            MainContent = Kernel.Resolve(message.PageOrViewModelType);
        }
    }
}