﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using TotallyAwesomePOS.UI.Model;

namespace TotallyAwesomePOS.UI.ViewModel
{
    public class HomeViewModel : ViewModelBase
    {

        public Session Session { get; set; }

        public string CashierName
        {
            get { return Session.Cashier.Name; }
        }

       

    }
}
