﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Castle.MicroKernel;
using TotallyAwesomePOS.UI.ViewModel;

namespace TotallyAwesomePOS.UI.Pages
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        public LoginPage()
        {
            InitializeComponent();
        }


        // Implements IPasswordProvider
        // I also made it an inner class so it would have access to the password
        // input variable.
        public class LoginPasswordProvider : IPasswordProvider
        {
            // I have to use the kernel to do a little dancing around 
            // the cyclical dependency chain between LoginPage and LoginViewModel
            private IKernel _kernel;

            public LoginPasswordProvider(IKernel kernel)
            {
                _kernel = kernel;
            }

            public string GetPassword()
            {
                // Get the LoginPage from the kernel to delay the dependency on it
                return _kernel.Resolve<LoginPage>().ThePassword.Password;
            }
        }
    }
}
