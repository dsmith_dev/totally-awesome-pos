﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Castle.Windsor;
using Castle.Windsor.Installer;
using GalaSoft.MvvmLight.Messaging;
using TotallyAwesomePOS.UI.Messages;
using TotallyAwesomePOS.UI.ViewModel;

namespace TotallyAwesomePOS.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        public IWindsorContainer IoC { get; private set; }

        private void Application_Startup(object sender, StartupEventArgs e)
        {

            IoC = new WindsorContainer()
                .Install(FromAssembly.This());

            var mainWindow = IoC.Resolve<MainWindow>();
            mainWindow.Show();

            IoC.Resolve<IMessenger>().Send(Navigate.ToViewModel<LoginViewModel>(from: this));
        }
    }
}
