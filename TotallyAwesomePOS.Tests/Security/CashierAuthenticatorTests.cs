﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;
using Rhino.Mocks;
using Ploeh.AutoFixture.NUnit3;

using TotallyAwesomePOS.Data;

namespace TotallyAwesomePOS.Security
{

    public class CashierAuthenticatorTests
    {

        [TestFixture]
        public class TheAuthenticateCashierMethod
        {
            
            [Test, AutoData]
            public void ShouldGetTheCashierFromTheDatabase(Cashier cashier)
            {
                var authenticator = new TestableCashierAuthenticator();
                authenticator
                    .CashierStorage
                    .Stub(x => x.Get("123"))
                    .Return(cashier);
                authenticator
                    .PasswordValidator
                    .Stub(x => x.ValidatePassword(Arg<string>.Is.Anything, Arg<string>.Is.Anything))
                    .Return(true);

                var c = authenticator.Authenticate("123", "test");

                Assert.That(c, Is.SameAs(cashier));
            }

            [Test, AutoData]
            public void ShouldValidateThePassword(Cashier cashier)
            {
                var authenticator = new TestableCashierAuthenticator();

                var c = authenticator.Authenticate("123", "test");

                authenticator
                    .PasswordValidator
                    .AssertWasCalled(x => x.ValidatePassword("123", "test"));
            }

            [Test, AutoData]
            public void ShouldReturnNullIfValidatePasswordReturnsFalse(Cashier cashier)
            {
                var authenticator = new TestableCashierAuthenticator();
                authenticator
                    .CashierStorage
                    .Stub(x => x.Get(Arg<string>.Is.Anything))
                    .Return(cashier);
                authenticator
                    .PasswordValidator
                    .Stub(x => x.ValidatePassword(Arg<string>.Is.Anything, Arg<string>.Is.Anything))
                    .Return(false);

                var c = authenticator.Authenticate("123", "test");

                Assert.That(c, Is.Null);
            }

        }

        public class TestableCashierAuthenticator : CashierAuthenticator
        {
            public TestableCashierAuthenticator()
            {
                CashierStorage = MockRepository.GenerateMock<IDataStorage<Cashier>>();
                PasswordValidator = MockRepository.GenerateMock<IPasswordValidator>();
            }
        }

    }

}
