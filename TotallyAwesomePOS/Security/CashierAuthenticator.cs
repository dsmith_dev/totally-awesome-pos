﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TotallyAwesomePOS.Data;

namespace TotallyAwesomePOS.Security
{
    public class CashierAuthenticator : ICashierAuthenticator
    {

        public IDataStorage<Cashier> CashierStorage { get; set; }

        public IPasswordValidator PasswordValidator { get; set; }

        public Cashier Authenticate(string username, string password)
        {
            var cashier = CashierStorage.Get(username);

            if (!PasswordValidator.ValidatePassword(username, password))
            {
                return null;
            }

            return cashier;
        }


    }
}
