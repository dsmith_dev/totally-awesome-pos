﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotallyAwesomePOS.Security
{
    public interface IPasswordValidator
    {

        bool ValidatePassword(string key, string password);

    }
}
