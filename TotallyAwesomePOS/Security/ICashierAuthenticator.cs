﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotallyAwesomePOS.Security
{
    public interface ICashierAuthenticator
    {
        Cashier Authenticate(string username, string password);
    }
}
