﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotallyAwesomePOS.Data
{
    public interface IDataStorage<T>
    {

        IQueryable<T> Queryable { get; }

        T Get(object id);

        void Add(T item);

        void Update(T item);

        void Delete(object id);

    }
}
